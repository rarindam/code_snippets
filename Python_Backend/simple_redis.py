#!/bin/bash/python3
import redis
import random

r = redis.Redis(host='localhost', port=6379, db=0)
pipe = r.pipeline()
pipe.set ('Orange', 'Hello')
pipe.set ('Apple', 'Hello1')

pipe.keys('*un*')

print (pipe.execute())

pipe.exists('Apple1')
a = pipe.execute()
print (a)
if (a[0] == 1):
	print ('Apple1 is in set')
else:
	print ('Apple1 is not in set')

for i in range(10):
	pipe.sadd('simpleset', i)
pipe.execute()
a = pipe.smembers('simpleset').execute()
print (a)

for i in range(10):
	if (r.exists('simpleset', i)):
		print ("Key found:" + str(i));
	else:
		print ("Key not found:" + str(i));

#Using a pipeline to execute in one go and save the results in array
for i in range(16):
	pipe.sismember('simpleset', i)

a = pipe.execute()
print (type(a))
print (a)

for i in range(16):
	if(a[i] == 1):
		print ("Key " + str(i) + " found in simpleset")
	else:
		print ("Key " + str(i) + " not found in simpleset")

for x in range (10):
	t = random.randint(1, 101)
	pipe.sadd('randomset', t)

pipe.execute()
print (r.smembers('randomset'))

for x in range (10):
	i = random.randint(1, 101)
	if (r.sismember('randomset', i)):
		print ("Key " + str(i) + " found in randomset")
	else:
		print ("Key " + str(i) + " not found in randomset")

r.flushall()
print (r.smembers('randomset'))
