from flask import Flask, jsonify, abort, make_response, json
import pymongo

app = Flask(__name__)

myclient = pymongo.MongoClient ("mongodb://localhost:27017")
mydb = myclient["mydatabase"]
mycollection = mydb["customers"]
print (myclient.list_database_names())

@app.route('/python/redis/listAll', methods=['GET'])
def get_mongo_data():
	l1 = list(mycollection.find({}, {"_id":0}))
	return jsonify({'whole_list':l1})
	
#	response = app.response_class( response=json.dumps(l1), status=200, mimetype='application/json')
#	return response

@app.errorhandler(404)
def not_found(error):
	return make_response(jsonify({'error': 'Not Found'}), 404)
@app.route('/')
def index():
	return "Hello, World\n"

if __name__ == '__main__':
	app.run(debug=True)

