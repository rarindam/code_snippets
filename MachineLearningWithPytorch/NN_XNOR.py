import torch
from torch import nn

"""
Implement the XNOR function using Andrew NG's
example.

Also used the following code, and vectorized it:
https://medium.com/convergeml/a-short-introduction-to-pytorch-using-logic-gates-in-perceptron-a8779fd93bd4
"""

X = torch.tensor([(1,0,0), (1,0,1), (1,1,0), (1,1,1)], dtype=torch.double)

def perceptronModel(Xt, W):
    #W=(3) and X=3X4. Result = 1x4
    W= W.resize_(1,3)
    Y = W.mm(Xt)

    #res = [[0 if i <= 0 else 1 for i in x] for x in Y]
    m = nn.SiLU()
    threshold = torch.tensor([0.0])

    output = m(Y)
    return (output>threshold).float()*1

def NOR_Logic(Xt):
    W = torch.tensor([1,-2,-2], dtype=torch.double)
    Y = perceptronModel(Xt, W)
    return Y

def AND_Logic(Xt):
    W = torch.tensor([-3,2,2], dtype=torch.double)
    Y = perceptronModel(Xt, W)
    return Y

def OR_Logic(Xt):
    W = torch.tensor([-1,2,2], dtype=torch.double)
    Y = perceptronModel(Xt, W)
    return Y

def XNOR_Logic(Xt):
    a1 = AND_Logic(Xt)

    a2 = NOR_Logic(Xt)

    a0 = torch.tensor([1,1,1,1], dtype=torch.double)
    #Convert to 1x4
    a0.resize_(1,4)

    # Make it (4x3)
    a3 = torch.cat((a0, a1, a2), dim=0)
    Y = OR_Logic(a3)

    return Y

def print_truth_table(res):
    x = [(0,0), (0,1), (1,0), (1,1)]
    l = res.tolist()

    for i in range(4):
        print (x[i], l[0][i])


if __name__=="__main__":

    res = XNOR_Logic(X.t())
    print("Printing XNOR table")
    print_truth_table(res)

